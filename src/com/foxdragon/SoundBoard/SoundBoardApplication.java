package com.foxdragon.SoundBoard;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SoundBoardApplication extends Application {

    private static final String TAG = "SoundBoardApplication";
    private static final String assetPath = "sounds";
    private static SoundBoardApplication sInstance;

    private AudioPlayer mAudioPlayer;
    private List<String> mSoundPaths;
    private Typeface mMarkerFelt;
    private SensorManager mSensorManager;
    private ShakeEventListener mSensorListener;

    public static SoundBoardApplication getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mAudioPlayer = new AudioPlayer();
        mSoundPaths = findSoundPaths();
        mMarkerFelt = Typeface.createFromAsset(getAssets(), "fonts/MarkerFelt.ttf");

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorListener = new ShakeEventListener();
        mSensorManager.registerListener(mSensorListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_UI);
    }

    public AudioPlayer getAudioPlayer() {
        return mAudioPlayer;
    }

    public List<String> getSoundPaths() {
        return mSoundPaths;
    }

    public Typeface getTypeface() {
        return mMarkerFelt;
    }

    public void setOnShakeListener(ShakeEventListener.OnShakeListener listener) {
        mSensorListener.setOnShakeListener(listener);
    }

    private List<String> findSoundPaths()
    {
        try {
            AssetManager assetManager = getAssets();
            String[] filePaths = assetManager.list(assetPath);
            List<String> retVal = new ArrayList<String>();
            for (String filePath: filePaths)
            {
                if (filePath.endsWith("mp3"))
                {
                    retVal.add(assetPath + "/" + filePath);
                }
            }
            return retVal;
        } catch (IOException ex) {
            return null;
        }
    }
}
