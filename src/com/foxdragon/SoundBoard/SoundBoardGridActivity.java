package com.foxdragon.SoundBoard;

import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class SoundBoardGridActivity extends AnimationBackgroundActivity implements AudioPlayer.OnPlaybackCompleted, ViewGroup.OnClickListener {
    private static final String TAG = "SoundBoardGridActivity";
    private AudioPlayer mAudioPlayer;
    private ViewPager mViewPager;
    private ToggleButton mActiveView;

    public SoundBoardGridActivity() {
        super(R.id.gridRootView);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sound_grid);

        TextView instructionTextView = (TextView)findViewById(R.id.instructionTextView);
        instructionTextView.setTypeface(SoundBoardApplication.getInstance().getTypeface());

        mAudioPlayer = SoundBoardApplication.getInstance().getAudioPlayer();
        mViewPager = (ViewPager)findViewById(R.id.pager);
        mViewPager.getBackground().setAlpha(123);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAudioPlayer.setOnPlaybackCompletedListener(this);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int displayWidth = displayMetrics.widthPixels;
        int soundWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, displayMetrics);

        SoundFragmentAdapter adapter = new SoundFragmentAdapter(getSupportFragmentManager(), displayWidth, soundWidth, SoundBoardApplication.getInstance().getSoundPaths());
        mViewPager.setAdapter(adapter);
    }

    @Override
    public void onPause()  {
        super.onPause();
        mAudioPlayer.stop();
    }

    void playSound(String soundPath) {
        mAudioPlayer.stop();
        startAnimation();
        mAudioPlayer.playAsset(soundPath);
    }

    public void onPlaybackCompleted() {
        stopAnimation();

        if (mActiveView != null) {
            mActiveView.setChecked(false) ;
        }

        Log.d(TAG, "Playback Completed");
    }

    @Override
    public void onClick(View view) {
        playSound(view.getTag().toString());

        if (view instanceof ToggleButton) {
            ToggleButton toggleButton = (ToggleButton)view;
            toggleButton.setChecked(true);
            mActiveView = toggleButton;
        }
    }
}