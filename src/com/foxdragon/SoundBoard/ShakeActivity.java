package com.foxdragon.SoundBoard;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

public class ShakeActivity extends AnimationBackgroundActivity implements ShakeEventListener.OnShakeListener {
    private AudioPlayer mAudioPlayer;
    private Random mRandom;

    public ShakeActivity() {
        super(R.id.shakeRootView);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shake);
        mRandom = new Random();
        mAudioPlayer = SoundBoardApplication.getInstance().getAudioPlayer();

        TextView instructionTextView = (TextView)findViewById(R.id.instructionTextView);
        instructionTextView.setTypeface(SoundBoardApplication.getInstance().getTypeface());
    }

    public void onResume() {
        super.onResume();

        SoundBoardApplication.getInstance().setOnShakeListener(this);

        mAudioPlayer.setOnPlaybackCompletedListener(new AudioPlayer.OnPlaybackCompleted() {
            @Override
            public void onPlaybackCompleted() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stopAnimation();
                    }
                });
            }
        });
    }

    @Override
    public void onPause()  {
        super.onPause();
        mAudioPlayer.stop();
        stopAnimation();
        SoundBoardApplication.getInstance().setOnShakeListener(null);
    }

    @Override
    public void onShake() {
        playRandomSound();
    }

    void playRandomSound() {
        mAudioPlayer.stop();
        startAnimation();
        List<String> audioPaths = SoundBoardApplication.getInstance().getSoundPaths();
        int randomIndex = mRandom.nextInt(audioPaths.size());
        mAudioPlayer.playAsset(audioPaths.get(randomIndex));
    }

}