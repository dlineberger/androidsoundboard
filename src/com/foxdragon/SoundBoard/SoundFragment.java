package com.foxdragon.SoundBoard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.File;
import java.util.List;

class SoundFragment extends Fragment {
    private static final String TAG = "SoundFragment";
    private final List<String> mSoundPaths;

    public SoundFragment(List<String> soundPaths)
    {
        super();
        mSoundPaths = soundPaths;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SoundBoardGridActivity activity = (SoundBoardGridActivity)getActivity();
        LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.sound_fragment, container, false);
        for (String soundPath : mSoundPaths) {
            View soundView = inflater.inflate(R.layout.sound_view, null);
            TextView textView = (TextView) soundView.findViewById(R.id.textView);
            if (textView != null) {
                textView.setText(getTitle(soundPath));
            }

            ToggleButton toggleButton = (ToggleButton)soundView.findViewById(R.id.imageButton);
            if (toggleButton != null) {
                toggleButton.setTag(soundPath);
                toggleButton.setOnClickListener(activity);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            layout.addView(soundView, layoutParams);
        }
        return layout;
    }

    private String getTitle(String soundPath) {
        File file = new File(soundPath);
        String name = file.getName();
        return name.substring(0, name.lastIndexOf('.'));
    }
}
