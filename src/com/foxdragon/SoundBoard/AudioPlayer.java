package com.foxdragon.SoundBoard;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class AudioPlayer implements SoundPool.OnLoadCompleteListener {
    private static final String TAG = "AudioPlayer";

    private final SoundPool mSoundPool;
    private final MediaPlayer mMediaPlayer;
    private float mPlaybackSpeed;
    private OnPlaybackCompleted mListener;
    private Timer mTimer;
    private long mDurationMs;

    public interface OnPlaybackCompleted {
        public void onPlaybackCompleted();
    }

    public AudioPlayer() {
        mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        mSoundPool.setOnLoadCompleteListener(this);

        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.start();
            }
        });

        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (mListener != null) {
                    mListener.onPlaybackCompleted();
                }
            }
        });
        mDurationMs = 0;
    }

    public void setOnPlaybackCompletedListener(OnPlaybackCompleted listener) {
        mListener = listener;
    }

    public void playAsset(String assetPath) {
        stop();

        AssetFileDescriptor afd;
        try {
            afd = SoundBoardApplication.getInstance().getAssets().openFd(assetPath);
            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mMediaPlayer.prepareAsync();
            afd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void play(String filePath, float playbackSpeed) {
        mPlaybackSpeed = playbackSpeed;

        try {
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(filePath);
            mMediaPlayer.prepare();
            mDurationMs = (long) (mMediaPlayer.getDuration() / playbackSpeed);
            Log.d(TAG, "Duration is " + mDurationMs + " ms");
            mMediaPlayer.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mSoundPool.load(filePath, 1);
    }

    public void stop() {
        if (mMediaPlayer.isPlaying())
        {
            mMediaPlayer.stop();
            if (mListener != null) {
                mListener.onPlaybackCompleted();
            }
        }
        mMediaPlayer.reset();
    }

    @Override
    public void onLoadComplete(SoundPool soundPool, int soundId, int status) {
        mSoundPool.play(soundId, 1.0f, 1.0f, 0, 0, mPlaybackSpeed);

        if (mTimer != null)
        {
            mTimer.cancel();
            mTimer = null;
        }

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onPlaybackCompleted();
                }
            }
        }, mDurationMs);
    }
}
