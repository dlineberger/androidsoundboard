package com.foxdragon.SoundBoard;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

class SoundFragmentAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "SoundFragmentAdapter";
    private final List<String> mSoundPaths;
    private final int mDisplayWidth;
    private final int mSoundWidth;

    public SoundFragmentAdapter(FragmentManager fragmentManager, int displayWidth, int soundWidth, List<String> soundPaths) {
        super(fragmentManager);
        mSoundPaths = soundPaths;
        mDisplayWidth = displayWidth;
        mSoundWidth = soundWidth;
    }

    @Override
    public Fragment getItem(int position) {
        double soundsPerFragment = Math.floor(mDisplayWidth / (float)mSoundWidth);
        int startingIndex = position * (int)soundsPerFragment;
        int endingIndex = (int)Math.min(mSoundPaths.size(), startingIndex + soundsPerFragment);
        return new SoundFragment(mSoundPaths.subList(startingIndex, endingIndex));
    }

    @Override
    public int getCount() {
        double soundsPerFragment = Math.floor(mDisplayWidth / (float) mSoundWidth);
        return (int)Math.ceil(mSoundPaths.size() / soundsPerFragment);
    }

}