package com.foxdragon.SoundBoard;

import android.graphics.drawable.AnimationDrawable;
import android.support.v4.app.FragmentActivity;
import android.view.View;

public class AnimationBackgroundActivity extends FragmentActivity {

    private int mResourceId;

    public AnimationBackgroundActivity(int resourceId) {
        super();
        mResourceId = resourceId;
    }

    @Override
    public void onPause()  {
        super.onPause();
        stopAnimation();
    }

    protected void stopAnimation() {
        View backgroundView = findViewById(mResourceId);
        if (!(backgroundView.getBackground() instanceof AnimationDrawable)) return;
        AnimationDrawable animationDrawable = (AnimationDrawable)backgroundView.getBackground();
        animationDrawable.stop();
        animationDrawable.selectDrawable(0);
    }

    protected void startAnimation() {
        View backgroundView = findViewById(mResourceId);
        if (!(backgroundView.getBackground() instanceof AnimationDrawable)) return;
        AnimationDrawable animationDrawable = (AnimationDrawable)backgroundView.getBackground();
        animationDrawable.start();
    }
}
