package com.foxdragon.SoundBoard;

import android.media.MediaRecorder;
import android.util.Log;

import java.io.IOException;

class AudioRecorder {
    private static final String TAG = "AudioRecorder";
    private MediaRecorder mRecorder;

    public void startRecording(String outputFileName) {
        if (mRecorder != null) {
            stopRecording();
        }

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile(outputFileName);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed", e);
        }

        Log.d(TAG, "Recording to " + outputFileName);

        mRecorder.start();
    }

    public void stopRecording() {
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }
    }
}
