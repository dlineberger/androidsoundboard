package com.foxdragon.SoundBoard;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.List;
import java.util.Random;


public class SoundBoardMainActivity extends AnimationBackgroundActivity implements ShakeEventListener.OnShakeListener {
    private AudioRecorder mAudioRecorder;
    private AudioPlayer mAudioPlayer;
    private Random mRandom;
    private String mRecordingFileName;
    private ImageButton mPlayButton;
    private ToggleButton mRecordButton;

    public SoundBoardMainActivity() {
        super(R.id.mainRootView);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mPlayButton = (ImageButton)findViewById(R.id.playimageButton);
        mRecordButton = (ToggleButton)findViewById(R.id.recordImageButton);

        TextView randomTextView = (TextView)findViewById(R.id.randomTextView);
        TextView gridTextView = (TextView)findViewById(R.id.gridTextView);
        TextView shakeTextView = (TextView)findViewById(R.id.shakeTextView);

        Typeface markerFelt = SoundBoardApplication.getInstance().getTypeface();
        randomTextView.setTypeface(markerFelt);
        gridTextView.setTypeface(markerFelt);
        shakeTextView.setTypeface(markerFelt);
        mRecordButton.setTypeface(markerFelt);


        mRecordButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    startRecording();
                } else {
                    stopRecording();
                }
            }
        });

        mAudioRecorder = new AudioRecorder();
        mAudioPlayer = SoundBoardApplication.getInstance().getAudioPlayer();
        mRandom = new Random();

        mRecordingFileName = getBaseContext().getFilesDir().getAbsolutePath() + "/soundboard.3gp";
    }

    @Override
    public void onResume() {
        super.onResume();

        SoundBoardApplication.getInstance().setOnShakeListener(this);

        SoundBoardApplication.getInstance().getAudioPlayer().setOnPlaybackCompletedListener(new AudioPlayer.OnPlaybackCompleted() {
            @Override
            public void onPlaybackCompleted() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stopAnimation();
                    }
                });
            }
        });
    }

    @Override
    public void onPause()  {
        super.onPause();
        mAudioPlayer.stop();
        stopAnimation();
        SoundBoardApplication.getInstance().setOnShakeListener(null);
    }

    public void onShake() {
        playRandomSound(null);
    }

    void startRecording() {
        mAudioRecorder.startRecording(mRecordingFileName);
        mPlayButton.setVisibility(View.VISIBLE);
    }

    void stopRecording() {
        mAudioRecorder.stopRecording();
        mRecordButton.setChecked(false);
    }

    @SuppressWarnings("unused")
    public void playRecording(View view) {
        stopRecording();
        mAudioPlayer.stop();
        startAnimation();
        mAudioPlayer.play(mRecordingFileName, 1.5f);
    }

    @SuppressWarnings("unused")
    public void playRandomSound(View view) {
        mAudioPlayer.stop();
        startAnimation();
        List<String> audioPaths = SoundBoardApplication.getInstance().getSoundPaths();
        int randomIndex = mRandom.nextInt(audioPaths.size());
        mAudioPlayer.playAsset(audioPaths.get(randomIndex));
    }

    @SuppressWarnings("unused")
    public void showSoundGridActivity(View view) {
        Intent intent = new Intent(this, SoundBoardGridActivity.class);
        startActivity(intent);
    }

    @SuppressWarnings("unused")
    public void showShakeActivity(View view) {
        Intent intent = new Intent(this, ShakeActivity.class);
        startActivity(intent);
    }
}
